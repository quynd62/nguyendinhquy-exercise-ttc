package lab02.n0804.tvshowtrailer.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import lab02.n0804.tvshowtrailer.R;
import lab02.n0804.tvshowtrailer.adapter.TvShowAdapter;
import lab02.n0804.tvshowtrailer.databinding.FragmentTvshowBinding;
import lab02.n0804.tvshowtrailer.interfacecontract.TvShowFragmentView;
import lab02.n0804.tvshowtrailer.model.ApiResponseShow;
import lab02.n0804.tvshowtrailer.model.ModelTvShowTest;
import lab02.n0804.tvshowtrailer.presenter.TvShowFragmentPresenter;
import lab02.n0804.tvshowtrailer.service.ConnectServer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TvShowFragment extends Fragment implements TvShowFragmentView {
    FragmentTvshowBinding binding;

    TvShowFragmentPresenter presenter;

    View view;
    RecyclerView recyclerViewTvShow;
    TvShowAdapter tvShowAdapter;
    ArrayList<ApiResponseShow.Show> listPopular = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_tvshow,container,false);
        presenter = new TvShowFragmentPresenter(this);
        presenter.getData();
        return binding.getRoot();
    }


    @Override
    public void onSuccessLoadData(List<ApiResponseShow.Show> list) {
        listPopular.addAll(list);
        tvShowAdapter = new TvShowAdapter(getActivity(),listPopular);
        binding.rcvTvShow.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        binding.rcvTvShow.setAdapter(tvShowAdapter);
    }
}
