package lab02.n0804.tvshowtrailer.presenter;

import androidx.recyclerview.widget.GridLayoutManager;

import lab02.n0804.tvshowtrailer.adapter.TvShowAdapter;
import lab02.n0804.tvshowtrailer.interfacecontract.TvShowFragmentAction;
import lab02.n0804.tvshowtrailer.interfacecontract.TvShowFragmentView;
import lab02.n0804.tvshowtrailer.model.ApiResponseShow;
import lab02.n0804.tvshowtrailer.service.ConnectServer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TvShowFragmentPresenter implements TvShowFragmentAction {
    private TvShowFragmentView mView;

    public TvShowFragmentPresenter(TvShowFragmentView mView) {
        this.mView = mView;
    }

    @Override
    public void getData() {
        //tien hanh call api de lay ve list cac tvshow
        ConnectServer.getApiService().getListShowPopular().enqueue(new Callback<ApiResponseShow>() {
            @Override
            public void onResponse(Call<ApiResponseShow> call, Response<ApiResponseShow> response) {
                if (response.isSuccessful()) {
                    ApiResponseShow apiResponseShow = response.body();
                    mView.onSuccessLoadData(apiResponseShow.getResults());
                }
            }

            @Override
            public void onFailure(Call<ApiResponseShow> call, Throwable t) {

            }
        });

    }
}
