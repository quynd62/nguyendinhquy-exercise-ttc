package lab02.n0804.tvshowtrailer.interfacecontract;

import java.util.List;

import lab02.n0804.tvshowtrailer.model.ApiResponseShow;

public interface TvShowFragmentView {
    void onSuccessLoadData(List<ApiResponseShow.Show> list);
}
