package lab02.n0804.tvshowtrailer.room;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {TvShowFavorite.class},version = 1)
public abstract class DBConnection extends RoomDatabase {
    public static DBConnection instance;
    public static synchronized DBConnection getDataBase(Context context){
            if (instance == null){
                instance = Room.databaseBuilder(context.getApplicationContext(),DBConnection.class,"database.db")
                        .allowMainThreadQueries()
                        .fallbackToDestructiveMigration()
                        .build();
            }
            return instance;
    }
    public abstract TvShowFavoriteDAO createTvShowFavDAO();
}
