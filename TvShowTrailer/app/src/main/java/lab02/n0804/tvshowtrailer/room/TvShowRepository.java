package lab02.n0804.tvshowtrailer.room;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

public class TvShowRepository {
    private TvShowFavoriteDAO tvShowFavoriteDAO;
    private LiveData<List<TvShowFavorite>> listFavorite;

    public TvShowRepository(Application application){
        DBConnection db = DBConnection.getDataBase(application);
        tvShowFavoriteDAO = db.createTvShowFavDAO();
        listFavorite = tvShowFavoriteDAO.list();
    }
    public LiveData<List<TvShowFavorite>> getAllShowFav(){
        return tvShowFavoriteDAO.list();
    }
    public void deleteTvShowFav(TvShowFavorite tvShowFavorite){
        tvShowFavoriteDAO.delete(tvShowFavorite);
    }
    public void addTvShowFav(TvShowFavorite tvShowFavorite){
        tvShowFavoriteDAO.insert(tvShowFavorite);
    }
    public TvShowFavorite getTvShowById(int idShow){
        return tvShowFavoriteDAO.getTvShowById(idShow);
    }
}
