package lab02.n0804.tvshowtrailer.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import lab02.n0804.tvshowtrailer.R;
import lab02.n0804.tvshowtrailer.adapter.MainViewPagerAdapter;
import lab02.n0804.tvshowtrailer.databinding.ActivityMainBinding;
import lab02.n0804.tvshowtrailer.fragment.FavoriteFragment;
import lab02.n0804.tvshowtrailer.fragment.TvShowFragment;

public class MainActivity extends AppCompatActivity {
    ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        setContentView(binding.getRoot());
        init();
    }

    private void init() {
        MainViewPagerAdapter adapter = new MainViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new TvShowFragment(),"TvShow");
        adapter.addFragment(new FavoriteFragment(),"Favorite");
        binding.viewpagerMain.setAdapter(adapter);
        binding.tablayoutMain.setupWithViewPager(binding.viewpagerMain);
        binding.tablayoutMain.getTabAt(0).setIcon(R.drawable.ic_tvshow);
        binding.tablayoutMain.getTabAt(1).setIcon(R.drawable.ic_heart);

    }


}