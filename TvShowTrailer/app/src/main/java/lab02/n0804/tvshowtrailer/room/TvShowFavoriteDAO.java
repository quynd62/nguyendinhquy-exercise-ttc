package lab02.n0804.tvshowtrailer.room;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface TvShowFavoriteDAO {
    @Insert
    void insert(TvShowFavorite tvShowFavorite);
    @Delete
    void delete(TvShowFavorite tvShowFavorite);

    @Query("SELECT * FROM tvshowfavorite")
    LiveData<List<TvShowFavorite>> list();

    @Query("SELECT * FROM tvshowfavorite WHERE idTvShow = :itemId")
    TvShowFavorite getTvShowById(int itemId);
}
