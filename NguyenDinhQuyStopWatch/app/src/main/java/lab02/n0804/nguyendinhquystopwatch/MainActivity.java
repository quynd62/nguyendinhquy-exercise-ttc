package lab02.n0804.nguyendinhquystopwatch;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tvTime;
    Button btnStart,btnPause,btnStop;
    long startTime,pauseTime,stopTime,systemTime = 0L;
    Handler handler = new Handler();
    boolean isRun;

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            systemTime = SystemClock.uptimeMillis() - startTime;
            long lUpdateTime = pauseTime + systemTime;
            long secs = (long)(lUpdateTime/1000);
            long mins= secs/60;
            secs = secs %60;
            long milliseconds = (long)(lUpdateTime%1000);
            tvTime.setText(""+mins+":" + String.format("%02d",secs) + ":" + String.format("%03d",milliseconds));
            handler.postDelayed(this,0);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvTime = findViewById(R.id.tvTime);
        btnStart = findViewById(R.id.btn_start);
        btnPause = findViewById(R.id.btn_pause);
        btnStop = findViewById(R.id.btn_stop);


    }

    @Override
    protected void onStart() {
        super.onStart();

        btnStart.setOnClickListener(this);
        btnPause.setOnClickListener(this);
        btnStop.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_start:
                if(isRun)
                    return;
                isRun = true;
                startTime = SystemClock.uptimeMillis();
                handler.postDelayed(runnable, 0);
                break;
            case R.id.btn_pause:
                if(!isRun)
                    return;
                isRun = false;
                pauseTime += systemTime;
                handler.removeCallbacks(runnable);
                break;
            case R.id.btn_stop:
                if(!isRun)
                    return;
                isRun = false;
                pauseTime = 0;
                handler.removeCallbacks(runnable);
                tvTime.setText("00:00:00");
                break;
        }
    }
}