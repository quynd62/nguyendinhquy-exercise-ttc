package lab02.n0804.nguyendinhquydowloadfile;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText edtURl;
    Button btnDowload;
    BroadcastReceiver dowloadReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(MainActivity.this, "dowload success", Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtURl = findViewById(R.id.edtURl);
        btnDowload = findViewById(R.id.btnDowload);
        btnDowload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtURl.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this, "Bạn hãy nhập URL trước khi dowload", Toast.LENGTH_SHORT).show();
                }else {
                    dowloadData(edtURl.getText().toString());
                }
            }
        });
    }
    public long dowloadData(String url){
        long output;

        DownloadManager downloadManager = (DownloadManager)getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
//        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI|DownloadManager.Request.NETWORK_MOBILE);
//        request.setAllowedOverRoaming(false);
        request.setTitle("dowload mp3");
        request.setDescription("demo dowloadfile");
//        request.setVisibleInDownloadsUi(true);
        request.setDestinationInExternalFilesDir(MainActivity.this,Environment.DIRECTORY_DOWNLOADS,"test_"+ SystemClock.uptimeMillis()+".mp3");
        output = downloadManager.enqueue(request);

        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        registerReceiver(dowloadReceiver, filter);
        return output;

    }
}